import networkx as nx
import numpy as np
from networkx.algorithms import bipartite
import matplotlib.pyplot as plt
import graph_operations as go
import run, math
import data_generate as dg

def sample_assign100(fName):
    A, G = go.create_graph_from_text(fName)
    M = bipartite.minimum_weight_full_matching(G)
    sumVal = go.compute_sum_from_matching(A, M)
    return sumVal, A, M, G

def sample_error(A_o):
    G = go.create_graph_from_numpy(A_o)
    M = bipartite.minimum_weight_full_matching(G)
    return  M, G

# Creates a list of Matrix A type matrix 
def create_pred_matrixA(shape, error_li = [0.1, 0.2, 0.3, 0.4, 0.5]):
    matA = []
    for i in error_li:
        arr = dg.generate_matrixA(shape)
        err_arr = dg.perturb_matrixA(arr, i)
        matA.append(err_arr)
    return matA

def plot_brunel_error():
    file_name_list = ["../test/assign100.txt", "../test/assign200.txt", "../test/assign300.txt", "../test/assign400.txt", "../test/assign500.txt", "../test/assign600.txt", "../test/assign700.txt", "../test/assign800.txt"]
    mu = 0.5
    ave0 = go.iterate_for_different_error(file_name_list, 0, mu)
    ave01 = go.iterate_for_different_error(file_name_list, 0.1, mu)
    ave02 = go.iterate_for_different_error(file_name_list, 0.2, mu)
    ave03 = go.iterate_for_different_error(file_name_list, 0.3, mu)
    ave04 = go.iterate_for_different_error(file_name_list, 0.4, mu)
    ave05 = go.iterate_for_different_error(file_name_list, 0.5, mu)
    print(ave0, ave01, ave02, ave03, ave04, ave05)
    x = np.linspace(100,800,8)
    plt.plot(x, ave0)
    plt.plot(x, ave01)
    plt.plot(x, ave02)
    plt.plot(x, ave03)
    plt.plot(x, ave04)
    plt.plot(x, ave05)
    plt.ylabel("solution quality")
    plt.xlabel("size (in hundreds - 100")
    plt.title("Brunel Error Graph")
    # y = (2*x)-1
    # plt.plot(x, y, 'g')
    # y = (np.log10(x))*(np.log10(x))
    # plt.plot(x, y, 'g')
    # y = (np.log(x))
    # plt.plot(x, y, 'g')
    plt.legend(['0','10%', '20%', '30%', '40%', '50%'])
    plt.savefig("../results/error_graphs/Brunel_error" + "_mu" + str(mu) + "SEED2" + ".png")
    plt.clf()
    return 

def plot_brunel_size():
    epsilon_list = [0, 0.1, 0.2, 0.3, 0.4, 0.5]
    mu = 0.5
    ave100 = go.iterate_for_increasing_n_graph("../test/assign100.txt", epsilon_list, mu)
    ave200 = go.iterate_for_increasing_n_graph("../test/assign200.txt", epsilon_list, mu)
    ave300 = go.iterate_for_increasing_n_graph("../test/assign300.txt", epsilon_list, mu)
    ave400 = go.iterate_for_increasing_n_graph("../test/assign400.txt", epsilon_list, mu)
    ave500 = go.iterate_for_increasing_n_graph("../test/assign500.txt", epsilon_list, mu)
    ave600 = go.iterate_for_increasing_n_graph("../test/assign600.txt", epsilon_list, mu)
    ave700 = go.iterate_for_increasing_n_graph("../test/assign700.txt", epsilon_list, mu)
    ave800 = go.iterate_for_increasing_n_graph("../test/assign800.txt", epsilon_list, mu)
    
    print("Solutions with Advice ")
    print(ave100, ave200, ave300, ave400, ave500, ave600)
    x = np.linspace(0,0.5,6)
    plt.plot(x, ave100)
    plt.plot(x, ave200)
    plt.plot(x, ave300)
    plt.plot(x, ave400)
    plt.plot(x, ave500)
    plt.plot(x, ave600)
    plt.plot(x, ave700)
    plt.plot(x, ave800)
    plt.legend(['size100','size200', 'size300', 'size400', 'size500', 'size600','size700','size800'])
    plt.ylabel("solution quality")
    plt.xlabel("error")
    plt.title("Brunel Size Graph")
    plt.savefig("../results/size_graphs/Brunel_size" + "_mu_" + str(mu) + "_SEED2" + ".png")
    plt.clf()
    return

def plot_brunel_error_2():
    file_name_list = ["../test/assign100.txt", "../test/assign200.txt", "../test/assign300.txt", "../test/assign400.txt", "../test/assign500.txt", "../test/assign600.txt", "../test/assign700.txt", "../test/assign800.txt"]
    ave0 = go.iterate_for_different_error_2(file_name_list, 0)
    ave01 = go.iterate_for_different_error_2(file_name_list, 0.1)
    ave02 = go.iterate_for_different_error_2(file_name_list, 0.2)
    ave03 = go.iterate_for_different_error_2(file_name_list, 0.3)
    ave04 = go.iterate_for_different_error_2(file_name_list, 0.4)
    ave05 = go.iterate_for_different_error_2(file_name_list, 0.5)
    print(ave0, ave01, ave02, ave03, ave04, ave05)
    plt.plot(ave0)
    plt.plot(ave01)
    plt.plot(ave02)
    plt.plot(ave03)
    plt.plot(ave04)
    plt.plot(ave05)
    plt.legend(['0','10%', '20%', '30%', '40%', '50%'])
    plt.ylabel("solution quality")
    plt.xlabel("size (in hundreds - 100")
    plt.title("Brunel Error Graph")
    plt.show()
    return 

def plot_brunel_size_2():
    err_list = [0, 0.1, 0.2, 0.3, 0.4, 0.5]
    ave100 = go.iterate_for_increasing_n_graph_2("../test/assign100.txt", err_list)
    ave200 = go.iterate_for_increasing_n_graph_2("../test/assign200.txt", err_list)
    ave300 = go.iterate_for_increasing_n_graph_2("../test/assign300.txt", err_list)
    ave400 = go.iterate_for_increasing_n_graph_2("../test/assign400.txt", err_list)
    ave500 = go.iterate_for_increasing_n_graph_2("../test/assign500.txt", err_list)
    ave600 = go.iterate_for_increasing_n_graph_2("../test/assign600.txt", err_list)
    ave700 = go.iterate_for_increasing_n_graph_2("../test/assign700.txt", err_list)
    ave800 = go.iterate_for_increasing_n_graph_2("../test/assign800.txt", err_list)
    
    print("Solutions with Advice ")
    print(ave100, ave200, ave300, ave400, ave500, ave600)
    plt.plot(ave100)
    plt.plot(ave200)
    plt.plot(ave300)
    plt.plot(ave400)
    plt.plot(ave500)
    plt.plot(ave600)
    plt.plot(ave700)
    plt.plot(ave800)
    plt.legend(['size100','size200', 'size300', 'size400', 'size500', 'size600','size700','size800'])
    plt.ylabel("solution quality")
    plt.xlabel("error (/10)")
    plt.title("Brunel Size Graph")
    plt.show()
    return

def plot_matrixA_error():
    size_list = [50, 75, 100, 125, 150, 175, 200]
    ave0 = go.iterate_for_different_error_matrixA(size_list, 0)
    ave01 = go.iterate_for_different_error_matrixA(size_list, 0.1)
    ave02 = go.iterate_for_different_error_matrixA(size_list, 0.2)
    ave03 = go.iterate_for_different_error_matrixA(size_list, 0.3)
    ave04 = go.iterate_for_different_error_matrixA(size_list, 0.4)
    ave05 = go.iterate_for_different_error_matrixA(size_list, 0.5)
    print(ave0, ave01, ave02, ave03, ave04, ave05)
    plt.plot(ave0)
    plt.plot(ave01)
    plt.plot(ave02)
    plt.plot(ave03)
    plt.plot(ave04)
    plt.plot(ave05)
    plt.legend(['0','10%', '20%', '30%', '40%', '50%'])
    plt.ylabel("solution quality")
    plt.xlabel("size (in hundreds - 100)")
    plt.title("MatrixA Error Graph")
    plt.show()
    return

def plot_matrixA_size_2():
    err_list = [0, 0.1, 0.2, 0.3, 0.4, 0.5]
    sol100 = go.iterate_for_increasing_n_matrixA_2(100, err_list)
    # sol200 = go.iterate_for_increasing_n_matrixA_2(200, err_list)
    # sol300 = go.iterate_for_increasing_n_matrixA_2(300, err_list)
    # sol400 = go.iterate_for_increasing_n_matrixA_2(400, err_list)
    # sol500 = go.iterate_for_increasing_n_matrixA_2(500, err_list)
    # sol600 = go.iterate_for_increasing_n_matrixA_2(600, err_list)
    # sol700 = go.iterate_for_increasing_n_matrixA_2(700, err_list)
    # sol800 = go.iterate_for_increasing_n_matrixA_2(800, err_list)
    
    # Individual
    fig1, ax = plt.subplots()
    ax.boxplot(sol100, labels=err_list) # Change file name
    plt.xlabel("error (/10)")
    plt.ylabel("solution quality")
    plt.savefig("../results/sol100_A_err_sq.png", dpi=600)
    plt.show()

    # All results in one picture
    # fig2, axs = plt.subplots(nrows=2, ncols=4, sharex=True, sharey=True, squeeze=False, figsize=(12, 6))
    # axs[0,0].boxplot(sol100, labels=err_list)
    # axs[0,1].boxplot(sol200, labels=err_list)
    # axs[0,2].boxplot(sol300, labels=err_list)
    # axs[0,3].boxplot(sol400, labels=err_list) 
    # axs[1,0].boxplot(sol500, labels=err_list)
    # axs[1,1].boxplot(sol600, labels=err_list)
    # axs[1,2].boxplot(sol700, labels=err_list) 
    # axs[1,3].boxplot(sol800, labels=err_list)
    # fig2.text(0.5, 0.04, 'error (/10)', ha='center')
    # fig2.text(0.04, 0.5, 'solution quality', va='center', rotation='vertical')
    # plt.savefig("../results/sol_A_err_sq.png", dpi=600)
    # plt.show()

    return

# def create_sample_brunel():



def main():
    # sumVal, A, M, G = sample_assign100("../test/assign100.txt")
    # A_o, _ = dg.perturb_choice(A, 0.5, 100)
    # M_o, _ = sample_error(A_co)
    # choice = run.generate_choice_matrix(A_o, M_o)
    # pred_sum = np.sum(np.multiply(A, choice))
    # print(pred_sum)
    # sol0, sol1, sol2, sol3, sol4, sol5 = [], [], [], [], [], []
    # arr, graph = go.create_graph_from_text("../test/assign200.txt")
    # optimal_offline = go.compute_from_numpy(arr)
    # print("Optimal solution from Karp: ", optimal_offline[0])
   
    plot_brunel_error()
    plot_brunel_size()
    # plot_matrixA_size()
    # plot_matrixA_error()
    # plot_brunel_error_2()
    # plot_brunel_size_2()

    # plot_brunel_size_2()
    # plot_brunel_error_2()

if __name__ == "__main__":
    main()  
