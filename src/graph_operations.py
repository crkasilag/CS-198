import networkx as nx
import numpy as np
from networkx.algorithms import bipartite
import matplotlib.pyplot as plt
import data_generate as dg

def create_graph_from_text(file_name):
    f = open(file_name)
    n = int(f.readline())

    A = np.array([]).astype(int)
    for line in f:
        A = np.append(A, list(map(int, line.strip().split(" "))))
    A = np.array(A).reshape(n,n)

    G = nx.Graph()
    G.add_nodes_from(list(range(n)), bipartite=0)
    G.add_nodes_from(list(range(n,n*2)), bipartite=1)
    for i in range(n):
        G.add_weighted_edges_from([(i, x, A[i][x-n]) for x in range(n,n*2)])
    
    # for line in bipartite.generate_edgelist(G):
    #     print(line)
    # print(minimum_weight_full_matching(G))
    
    # Visualization
    # l,r = nx.bipartite.sets(G)
    # pos = nx.bipartite_layout(G, l)
    # nx.draw(G, with_labels=True, pos=pos)
    # plt.show()

    f.close()

    return A, G

# Included this for numpy matrices
def create_graph_from_numpy(array):
    G = nx.Graph()
    n = array.shape[0]
    G.add_nodes_from(list(range(n)), bipartite=0)
    G.add_nodes_from(list(range(n,n*2)), bipartite=1)
    for i in range(n):
        G.add_weighted_edges_from([(i, x, array[i][x-n]) for x in range(n,n*2)])

    return G

def compute_from_numpy(array):
    G = create_graph_from_numpy(array)
    M = bipartite.minimum_weight_full_matching(G)
    return compute_sum_from_matching(array, M) # returns sum and zeros

def compute_sum_from_matching(A, M):
    n = A.shape[0]
    zeros = np.zeros([A.shape[0], A.shape[1]])
    sum = 0

    for i in range(n):
        sum = sum + A[i][M[i] - n]
        zeros[i][M[i]-n]=1

    return sum, zeros

def run_projection(arr, error, iter_):
    sol = []
    for i in range(iter_):
        pred_arr = dg.perturb_choice(arr, error)
        sum, zeros = compute_from_numpy(pred_arr)
        sum_from_projection = np.sum(np.multiply(arr, zeros))
        sol.append(sum_from_projection)
    return sol

def run_projection_2(arr, error, iter_):
    sol = []
    for i in range(iter_):
        pred_arr = dg.perturb_choice_2(arr, error, 10)
        sum, zeros = compute_from_numpy(pred_arr)
        sum_from_projection = np.sum(np.multiply(arr, zeros))
        sol.append(sum_from_projection)
    return sol

def iterate_for_increasing_n_graph(file_name, err_list):
    ave = []
    arr, graph = create_graph_from_text(file_name)
    optimal_offline = compute_from_numpy(arr)
    print("Optimal solution from Karp: ", optimal_offline[0])
    for i in err_list:
        sol = run_projection(arr, i, 3)
        ave.append(np.mean(sol)/optimal_offline[0])
    return ave

def iterate_for_increasing_n_graph_2(file_name, err_list):
    sol_set = []
    arr, graph = create_graph_from_text(file_name)
    optimal_offline = compute_from_numpy(arr)
    print("Optimal solution from Karp: ", optimal_offline[0])
    for i in err_list:
        sol = run_projection(arr, i, 3)
        sol_set.append(np.array(sol)/optimal_offline[0])
    return sol_set

def iterate_for_different_error(file_name_list, err):
    ave = []
    for i in file_name_list:
        arr, graph = create_graph_from_text(i)
        optimal_offline = compute_from_numpy(arr)
        sol = run_projection(arr, err, 3)
        ave.append(np.mean(sol)/optimal_offline[0])
    print ("done")
    return ave

def iterate_for_increasing_n_graph_2(file_name, err_list):
    ave = []
    arr, graph = create_graph_from_text(file_name)
    optimal_offline = compute_from_numpy(arr)
    print("Optimal solution from Karp: ", optimal_offline[0])
    for i in err_list:
        sol = run_projection_2(arr, i, 3)
        ave.append(np.mean(sol)/optimal_offline[0])
    return ave

def iterate_for_different_error_2(file_name_list, err):
    ave = []
    for i in file_name_list:
        arr, graph = create_graph_from_text(i)
        optimal_offline = compute_from_numpy(arr)
        sol = run_projection_2(arr, err, 3)
        ave.append(np.mean(sol)/optimal_offline[0])
    print ("done")
    return ave

def run_projection_matrixA(arr, error, iter_):
    sol = []
    for i in range(iter_):
        pred_arr = dg.perturb_matrixA(arr, error)
        sum, zeros = compute_from_numpy(pred_arr)
        sum_from_projection = np.sum(np.multiply(arr, zeros))
        sol.append(sum_from_projection)
    return sol

def iterate_for_increasing_n_matrixA(size, err_list):
    ave = []
    arr = dg.generate_random_12(size)
    graph = create_graph_from_numpy(arr)
    optimal_offline = compute_from_numpy(arr)
    print("Optimal solution from Karp: ", optimal_offline[0])
    for i in err_list:
        sol = run_projection_matrixA(arr, i, 3)
        ave.append(np.mean(sol)/optimal_offline[0])
    return ave

def iterate_for_increasing_n_matrixA_2(size, err_list):
    ave = []
    arr = dg.generate_random_12(size)
    graph = create_graph_from_numpy(arr)
    optimal_offline = compute_from_numpy(arr)
    print("Optimal solution from Karp: ", optimal_offline[0])
    for i in err_list:
        sol = run_projection_matrixA(arr, i, 3)
        ave.append(np.array(sol)/optimal_offline[0])
    return ave

def iterate_for_different_error_matrixA(size_list, err):
    ave = []
    for i in size_list:
        arr = dg.generate_random_12(i)
        graph = create_graph_from_numpy(arr)
        optimal_offline = compute_from_numpy(arr)
        sol = run_projection_matrixA(arr, err, 3)
        ave.append(np.mean(sol)/optimal_offline[0])
    print ("done")
    return ave
