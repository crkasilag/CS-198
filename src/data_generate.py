import random
import numpy as np  
import math

# Generate an upper triangular of 1s with shape (x,x) 
def generate_matrixA(x): 
    if(x<=0):
        raise Exception("You cannot create a matrix with zero or negative shape.")
    else:
        arr = np.ones([x,x])
        arr[np.tril_indices(arr.shape[0], -1)] = 2
    return arr  

# Generate a random matrix of 1s and 2s with randint (distribution to follow) with diagonals set to 1s
def generate_random_12(size, seed=0):
    arr = np.random.randint(1,high=3, size=(size,size))
    np.fill_diagonal(arr, 1)
    return arr

def get_error_choices(shape, err):
    error_indices = np.random.choice(range(shape), err, replace=True)



# Perturb a matrix arr of 1s and 2s given an integer arr
def perturb_matrixA(arr, error, seed=0): 
    err = math.floor(arr.shape[0]*arr.shape[0]*error)
    # print(err)
    vals = arr[np.array(arr, dtype=bool)]
    err_indices = np.random.choice(range(vals.shape[0]), int(err), replace=False)
    # print(err_indices)
    # HARDCODED
    for i in err_indices:
        if (vals[i] == 1):
           vals[i] = 2
        else: 
          vals[i] = 1
    arr[np.array(arr, dtype=bool)] = vals

    return np.abs(arr)

# This assumes static scale of error. 
# We can also specific min/max here
def perturb_choice(arr_inp, epsilon, mu, seed=0):
    np.random.seed(seed) 
    arr = np.copy(arr_inp)
    max_val = np.amax(arr)
    err = math.floor(arr.shape[0]*arr.shape[0]*epsilon)
    vals = arr[np.array(arr, dtype=bool)]
    err_indices = np.random.choice(range(vals.shape[0]), int(err), replace=False)
    for i in err_indices:
        if ((vals[i] + (mu*max_val)) > max_val):
            vals[i] = int(math.floor(vals[i] - mu * max_val))
            # print((vals[i] + (err*max_val)))
        elif ((vals[i] - (mu*max_val)) < 0):
            vals[i] = int(math.floor(vals[i] + mu * max_val))
            # print(vals[i])
        else:
            if(np.random.rand() > 0.5):
                vals[i] = int(math.floor(vals[i] + mu * max_val))
            else:
                vals[i] = int(math.floor(vals[i] - mu * max_val))
    arr[np.array(arr, dtype=bool)] = vals

    return arr

def perturb_choice_2(arr_inp, error, val, seed=0): 
    arr = np.copy(arr_inp)
    err = math.floor(arr.shape[0]*arr.shape[0]*error)
    vals = arr[np.array(arr, dtype=bool)]
    err_indices = np.random.choice(range(vals.shape[0]), int(err), replace=False)
    for i in err_indices:
        if ((vals[i] + val) > np.amax(arr)):
            vals[i] = int(math.floor(vals[i] - val))
            # print((vals[i] + (err*max_val)))
        elif ((vals[i] - (val)) < np.amin(arr)):
            vals[i] = int(math.floor(vals[i] + val))
            # print(vals[i])
        else:
            if(np.random.rand() > 0.5):
                vals[i] = int(math.floor(vals[i] + val))
            else:
                vals[i] = int(math.floor(vals[i] - val))
    arr[np.array(arr, dtype=bool)] = vals

    return arr
    # selector = np.random.choice([1,0], size=(arr.shape[0], arr.shape[0]), p=[error, 1-error])
    # int_error = np.random.rand(arr.shape[0], arr.shape[0])
    # error_matrix = np.abs((np.multiply(selector, int_error)*max_val) + arr)
    # error_matrix[error_matrix>max_val] = error_matrix[error_matrix>max_val] - max_val
    # return np.floor(error_matrix), np.multiply(selector, int_error)

def perturb_choice_partition(arr_inp, error, seed):
    np.random.seed(seed)
    arr = np.copy(arr_inp).flatten()
    err = int(arr.shape[0]*error)
    val_err = int(arr.sum()*error)
    err_indices = np.random.choice(range(arr.shape[0]), int(err), replace=False)
    if err != 0:
        x = val_err / err_indices.shape[0]
        for i in err_indices:
            arr[i] = arr[i] + x
    arr = np.reshape(arr, (arr_inp.shape[0], arr_inp.shape[1]))